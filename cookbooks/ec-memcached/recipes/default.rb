#
# Cookbook Name:: ec-memcached
# Recipe:: default
#
# Copyright (C) 2014 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#

execute "set memcached memory" do
    command "perl -pi -e 's/-m.*/-m 400/g' /etc/memcached.conf"
end

service "memcached" do
    action :restart
end
