name             'ec-memcached'
maintainer       'EC Developers'
maintainer_email 'webteam@ecenglish.com'
license          'All rights reserved'
description      'Configures available memory for memcached.'
long_description 'Configures available memory for memcached.'
version          '0.0.1'

