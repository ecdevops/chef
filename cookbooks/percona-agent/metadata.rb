name             'percona-agent'
maintainer       'Iain Mckay'
maintainer_email 'me@iainmckay.co.uk'
license          'Apache 2.0'
description      'Installs/Configures percona-agent'
long_description 'Installs/Configures percona-agent'
version          '1.0.0'

