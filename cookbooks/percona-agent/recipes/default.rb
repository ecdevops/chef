#
# Cookbook Name:: percona-agent
# Recipe:: default
#
# Copyright 2014, Iain Mckay
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


remote_file "agent_archive" do
    path "/tmp/percona-agent.tar.gz"
    source node['percona-agent']['archive']
    action :create_if_missing
end

execute "mkdir /tmp/percona-agent" do
    action :run
end

execute "tar -zxf /tmp/percona-agent.tar.gz -C /tmp/percona-agent --strip 1" do
  cwd "/tmp"
end

execute "./install -api-key=\"#{node['percona-agent']['api_key']}\" -basedir=\"#{node['percona-agent']['base_path']}\"" do
  cwd "/tmp/percona-agent"
end

execute "rm -rf /tmp/percona-agent" do
    action :run
end

execute "rm -f /tmp/percona-agent.tar.gz" do
    action :run
end
