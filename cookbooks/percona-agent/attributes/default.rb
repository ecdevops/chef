default['percona-agent']['archive'] = 'http://www.percona.com/downloads/percona-agent/LATEST/percona-agent-1.0.2-x86_64.tar.gz'
default['percona-agent']['api_key'] = nil
default['percona-agent']['base_path'] = '/usr/local/percona/percona-agent'
