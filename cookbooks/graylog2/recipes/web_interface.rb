#
# Cookbook Name:: graylog2
# Recipe:: web_interface
#
# Copyright 2010, Medidata Solutions Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Install required APT packages
package "build-essential"
if node.graylog2.email_package
    package node.graylog2.email_package
end

# Install gem dependencies
gem_package "bundler"
gem_package "rake"

# Create the release directory
directory "#{node.graylog2.basedir}/rel" do
  mode 0755
  recursive true
end

# Download the desired version of Graylog2 web interface from GitHub
remote_file "download_web_interface" do
  path "#{node.graylog2.basedir}/rel/graylog2-web-interface-#{node.graylog2.web_interface.version}.tgz"
  source "https://github.com/Graylog2/graylog2-web-interface/releases/download/#{node.graylog2.server.version}/graylog2-web-interface-#{node.graylog2.server.version}.tgz"
  action :create_if_missing
end

# Unpack the desired version of Graylog2 web interface
execute "tar zxf graylog2-web-interface-#{node.graylog2.web_interface.version}.tgz" do
  cwd "#{node.graylog2.basedir}/rel"
  creates "#{node.graylog2.basedir}/rel/graylog2-web-interface-#{node.graylog2.web_interface.version}/build_date"
  action :nothing
  subscribes :run, resources(:remote_file => "download_web_interface"), :immediately
end

# Link to the desired Graylog2 web interface version
link "#{node.graylog2.basedir}/web" do
  to "#{node.graylog2.basedir}/rel/graylog2-web-interface-#{node.graylog2.web_interface.version}"
end

external_hostname = node.graylog2.external_hostname     ? node.graylog2.external_hostname :
    (node.has_key?('ec2') and node.ec2.has_key?('public_hostname')) ? node.ec2.public_hostname :
    (node.has_key?('ec2') and node.ec2.has_key?('public_ipv4'))     ? node.ec2.public_ipv4 :
    node.has_key?('fqdn')                                           ? node.fqdn :
    "localhost"

# Create graylog2.conf
template "#{node.graylog2.basedir}/rel/graylog2-web-interface-#{node.graylog2.web_interface.version}/conf/graylog2-web-interface.conf" do
  source "graylog2-web-interface.conf.erb"
  mode 0755
end

# Create init.d script
template "/etc/init.d/graylog2-web-interface" do
  source "graylog2-web-interface.init.erb"
  mode 0755
end

# Update the rc.d system
execute "update-rc.d graylog2-web-interface defaults" do
  creates "/etc/rc0.d/K20graylog2-web-interface"
  action :nothing
  subscribes :run, resources(:template => "/etc/init.d/graylog2-web-interface"), :immediately
end

# Service resource
service "graylog2-web-interface" do
  supports :restart => false
  action [:enable, :start]
end
