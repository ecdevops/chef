#
# Cookbook Name:: php
# Recipe:: default
#
# Copyright (C) 2014 EC Holdings Ltd.
#
# All rights reserved - Do Not Redistribute
#

# use ondrej for php5
apt_repository 'ondrej-php5' do
    uri 'http://ppa.launchpad.net/ondrej/php5/ubuntu'
    distribution node['lsb']['codename']
    components ['main']
    keyserver 'keyserver.ubuntu.com'
    key 'E5267A6C'
    cache_rebuild false
end

# adding the repository above can be done while a background process has a lock on apt, so the update fails
# loop here until apt becomes available and do an update
execute "apt-update" do
    command "apt-get update"
    retries 10
    retry_delay 5
end

execute "rename LockFile directive" do
    command "perl -pi -e 's/.*LockFile.*/Mutex file:\${APACHE_LOCK_DIR} default/g' /etc/apache2/apache2.conf"
end

execute "install apache2-mpm-worker" do
    command "echo 'Y Y' | apt-get install -y apache2-mpm-worker"
end

execute "disable all apache2 modules" do
    command "rm /etc/apache2/mods-enabled/*"
end

if node['apache2']['modules']
    node['apache2']['modules'].each do |item|
        execute "enable apache2 #{item}" do
            command "a2enmod #{item}"
        end
    end
end

include_recipe 'php-fpm'

ruby_block "disable all php modules" do
    block do
        Dir.foreach('/etc/php5/mods-available') do |item|
            next if item == '.' or item == '..'
            item = File.basename(item, '.ini')

            system "php5dismod #{item}"
        end
    end
    action :run
end

if node['php']['modules']
    node['php']['modules'].each do |item|
        package = "php5-#{item.name}"
        execute "install #{package}" do
            command "apt-get install -y #{package}"
            not_if { item.has_key?('install') && item.install == false }
        end

        execute "enable #{item.name}" do
            command "php5enmod #{item.name}"
        end
    end
end

execute "symlink default-ssl.conf to default-ssl for scalr to work" do
    command "ln -s /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/default-ssl"
    not_if { ::File.exists?('/etc/apache2/sites-available/default-ssl') }
end

service "php5-fpm" do
    action :restart
end

service "apache2" do
    action :restart
    only_if { ::File.directory?('/etc/scalr/private.d/vhosts') }
end

# install logster
execute "update custom logster parsers" do
  command "git pull"
  cwd "/tmp/logster_extra_parsers"
  only_if { File.exists?("/tmp/logster_extra_parsers") }
end

execute "checkout custom logster parsers" do
  command "git clone https://git@bitbucket.org/ecenglish/logster.git /tmp/logster_extra_parsers"
  not_if { File.exists?("/tmp/logster_extra_parsers") }
end

include_recipe "logster::default"

Dir.glob("/var/log/http-*-datadog.log") do |logname|
  logster_log "#{logname}" do
    output "statsd"
    statsd_host "127.0.0.1:8125"
    parser "ApacheLogster.ApacheLogster"
  end
end
