#
# Cookbook Name:: base
# Recipe:: default
#
# Copyright (C) 2014 EC Holdings Ltd.
#
# All rights reserved - Do Not Redistribute
#

[2,3,4,5,6].each do |item|
  file = "/etc/init/tty#{item}.conf"

  execute "remove and kill #{item}" do
    command "rm #{file} && initctl stop tty#{item}"
    only_if { ::File.exists?(file) }
    ignore_failure true
  end
end

node['ec-base']['apt']['repositories'].each do |repo|
  apt_repository "#{repo.name}" do
    uri "#{repo.uri}"
    distribution node["lsb"]["codename"]
    components ["main"]
    keyserver "keyserver.ubuntu.com"
    key "#{repo.keyrepo}"
  end
end

execute "apt update" do
    command "apt-get update"
end

node['ec-base']['apt']['packages'].each do |pkg|
  package pkg
end

if node['ec-base']['nodejs']['packages'].length() > 0
  include_recipe "nodejs"

  node['ec-base']['nodejs']['packages'].each do |pkg|
    nodejs_npm "#{pkg.name}"
  end
end

node['ec-base']['gem']['packages'].each do |pkg|
  if pkg.has_key?('version')
    gem_package "#{pkg.name}" do
      version "#{pkg.version}"
      options pkg.has_key?('prerelease') ? "--pre" : ""
    end
  else
    gem_package "#{pkg.name}" do
      options pkg.has_key?('prerelease') ? "--pre" : ""
    end
  end
end

apt_package "git" do
  action :install
end

apt_package "libssl-dev" do
  action :install
end

apt_package "build-essential" do
  action :install
end

# Create init.d script
template "/etc/rsyslog.d/100-graylog2.conf" do
  source "rsyslog.graylog2.erb"
  mode 0755
end

# Service resource
service "rsyslog" do
  supports :restart => true
  action [:restart]
end
