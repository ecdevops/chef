#
# Cookbook Name:: base
# Recipe:: datadog
#
# Copyright (C) 2014 EC Holdings Ltd.
#
# All rights reserved - Do Not Redistribute
#

if node['ec-base']['datadog']['tcp_checks'].length > 0
  template "/etc/dd-agent/conf.d/tcp_check.yaml" do
    source "tcp_check.erb"
    mode 0755
  end
end

if node['ec-base']['datadog']['http_checks'].length > 0
  template "/etc/dd-agent/conf.d/http_check.yaml" do
    source "http_check.erb"
    mode 0755
  end
end

service "datadog-agent" do
  supports :restart => true
  action [:restart]
end
