#
# Cookbook Name:: php
# Recipe:: default
#
# Copyright (C) 2014 EC Holdings Ltd.
#
# All rights reserved - Do Not Redistribute
#

# use ondrej for php5
apt_repository "ondrej-php5" do
    uri "http://ppa.launchpad.net/ondrej/php5/ubuntu"
    distribution node["lsb"]["codename"]
    components ["main"]
    keyserver "keyserver.ubuntu.com"
    key "E5267A6C"
    cache_rebuild false
end

# adding the repository above can be done while a background process has a lock on apt, so the update fails
# loop here until apt becomes available and do an update
execute "apt-update" do
    command "apt-get update"
    retries 10
    retry_delay 5
end

include_recipe "php-fpm"

ruby_block "disabling all php modules" do
    block do
        Dir.foreach("/etc/php5/mods-available") do |item|
            next if item == "." or item == ".."
            item = File.basename(item, ".ini")

            system "php5dismod #{item}"
        end
    end
    action :run
end

if node["ecenglish"]["php"]["modules"]
    node["ecenglish"]["php"]["modules"].each do |item|
        package = "php5-#{item.name}"
        execute "install #{package}" do
            command "apt-get install -y #{package}"
            not_if { item.has_key?("install") && item.install == false }
        end

        execute "enabling #{item.name}" do
            command "php5enmod #{item.name}"
        end
    end
end

service "php5-fpm" do
    action :restart
end

include_recipe "composer::default"
