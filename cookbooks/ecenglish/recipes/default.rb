#
# Cookbook Name:: base
# Recipe:: default
#
# Copyright (C) 2014 EC Holdings Ltd.
#
# All rights reserved - Do Not Redistribute
#

node["ecenglish"]["apt"]["repositories"].each do |repo|
  apt_repository "#{repo.name}" do
    uri "#{repo.uri}"
    distribution node["lsb"]["codename"]
    components ["main"]
    keyserver "keyserver.ubuntu.com"
    #key "#{repo.key}"
  end
end

execute "apt update" do
  command "apt-get update"
end

node["ecenglish"]["apt"]["packages"].each do |pkg|
  package pkg
end

if node["ecenglish"]["nodejs"]["packages"].length() > 0
  include_recipe "nodejs"

  node["ecenglish"]["nodejs"]["packages"].each do |pkg|
    nodejs_npm "#{pkg.name}"
  end
end

node["ecenglish"]["gem"]["packages"].each do |pkg|
  if pkg.has_key?("version")
    gem_package "#{pkg.name}" do
      version "#{pkg.version}"
      options pkg.has_key?("prerelease") ? "--pre" : ""
    end
  else
    gem_package "#{pkg.name}" do
      options pkg.has_key?("prerelease") ? "--pre" : ""
    end
  end
end

# Create init.d script
#template "/etc/rsyslog.d/100-graylog2.conf" do
#  source "rsyslog.graylog2.erb"
#  mode 0755
#end

# Service resource
#service "rsyslog" do
#  supports :restart => true
#  action [:restart]
#end
