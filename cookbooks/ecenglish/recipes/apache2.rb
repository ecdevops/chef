#
# Cookbook Name:: apache2
# Recipe:: default
#
# Copyright (C) 2014 EC Holdings Ltd.
#
# All rights reserved - Do Not Redistribute
#

template "/etc/apache2/sites-available/app" do
  source "apache2.vhost.erb"
  mode 0755
end

apache_site "app"

#include_recipe "logster::default"
